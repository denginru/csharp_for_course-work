﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z1
{
    class Program
    {
        static void Main(string[] args)
        {
            double R, S, L;
            if (double.TryParse(Console.ReadLine(), out R))
            {
                if (R > 0)
                {
                    S = Math.PI * R * R;
                    L = 2 * Math.PI * R;
                    Console.WriteLine("Площадь окружности = {0}, длина = {1}", S, L);
                    if (S < L) Console.WriteLine("Ваша окружность хорошая");
                    else Console.WriteLine("Ваша окружность хорошей не является");
                }
                else
                    Console.WriteLine("Радиус должен быть положительным");
            }
            else
                Console.WriteLine("Плохой радиус. С ним нельзя работать");
            Console.ReadKey();
        }
    }
}
