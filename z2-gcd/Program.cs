﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z2_gcd
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, olda, oldb;
            
            int.TryParse(Console.ReadLine(), out a);
            int.TryParse(Console.ReadLine(), out b);

            olda=a; oldb=b;

            while (b>0)
            {
                c = a%b;
                a = b;
                b = c;
            }

            Console.WriteLine("Наибольший общий делитель числел {0} и {1} равен {2}", olda, oldb, a);

            Console.ReadKey();
        }
    }
}
