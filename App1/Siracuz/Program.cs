﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siracuz
{
    class Program
    {
        static void Main(string[] args)
        {
            int x0, n;
            
            Console.Write("x0=");
            int.TryParse(Console.ReadLine(), out x0);

            Console.Write("n=");
            int.TryParse(Console.ReadLine(), out n);

            if (n>0) Console.WriteLine(x0);

            for (int i=1; i < n; i++)
            {
                if (x0 % 2 == 0)
                    x0 = x0 / 2;
                else
                    x0 = (3 * x0 + 1) / 2;
                Console.WriteLine(x0);
            }
            Console.ReadKey();
        }
    }
}
