﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siracuz2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x0;

            Console.Write("x0=");
            int.TryParse(Console.ReadLine(), out x0);
            
            Console.WriteLine(x0);

            while (x0 != 1)
            {
                if (x0 % 2 == 0)
                    x0 = x0 / 2;
                else
                    x0 = (3 * x0 + 1) / 2;
                Console.WriteLine(x0);
            }
            Console.ReadKey();
        }
    }
}
