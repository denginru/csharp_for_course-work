﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Это комментарий
            
            /*
             * Это тоже комментарий
             * только многострочный
            */
            double radius=3;
            double l=2 * Math.PI * radius;
            //Можно разрывать определения
            Console.Write("Введите ваш возраст:");
            string s;
            s=Console.ReadLine();
            int a;
            int.TryParse(s, out a);
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}